# VSDL HW3 object detection

## Links
### Library
* keras-retinanet 0.5.0 https://github.com/fizyr/keras-retinanet

### Source code
* retinanet-digit-detector https://github.com/penny4860/retinanet-digit-detector

## Run

### Training
`python main.py`
* Backbone: `resnet50`, `resnet101`, `resnet152`
* Transform generator: use, don't use
* Train/Valid split: 0.8/0.2
* Load data: `retina/pascal.py`
* mAP threshhold: 0.8 (pascal style)

### Testing
`python infer.py`
* Backbone
* Model snapshot: `snapshot\*.h5`
* Save result: `*.json`