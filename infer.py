# -*- coding: utf-8 -*-

from keras_retinanet import models
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image

import matplotlib.pyplot as plt
import cv2
import os
import numpy as np
import time

from retina.utils import visualize_boxes

import glob
from tqdm import tqdm
import json

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"

#MODEL_PATH = 'snapshots/mod_resnet50_pascal_05.h5'
MODEL_PATH = 'snapshots/rand_resnet152_pascal_09.h5'

LABELS = ['10', '1', '2', '3', '4', '5', '6', '7', '8', '9']

def load_inference_model(model_path=os.path.join('snapshots', 'resnet.h5')):
    model = models.load_model(model_path, backbone_name='resnet152')
    model = models.convert_model(model)
    #model.summary()
    return model

def post_process(boxes, original_img, preprocessed_img):
    # post-processing
    h, w, _ = preprocessed_img.shape
    h2, w2, _ = original_img.shape
    boxes[:, :, 0] = boxes[:, :, 0] / w * w2
    boxes[:, :, 2] = boxes[:, :, 2] / w * w2
    boxes[:, :, 1] = boxes[:, :, 1] / h * h2
    boxes[:, :, 3] = boxes[:, :, 3] / h * h2
    return boxes


if __name__ == '__main__':
    
    model = load_inference_model(MODEL_PATH)
    name_list = [img for img in glob.glob("../test/*.png")]
    name_list.sort(key=lambda x: int(x.split('/')[2].split('.')[0]))
    output = []
    for img_name in tqdm(name_list):
        # load image
        image = read_image_bgr(img_name)

        # copy to draw on
        draw = image.copy()
        draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)

        # preprocess image for network
        image = preprocess_image(image)
        image, _ = resize_image(image, 416, 448)

        # process image
        #start = time.time()
        boxes, scores, labels = model.predict_on_batch(np.expand_dims(image, axis=0))
        #print("processing time: ", time.time() - start)
        boxes = post_process(boxes, draw, image)
        labels = labels[0]
        scores = scores[0]
        boxes = boxes[0]

        output_dict = {'bbox':[], 'label':[], 'score':[]}
        for i in range(len(scores)):
            if scores[i] >= 0.5:
                ymin, xmin, ymax, xmax = boxes[i]
                output_dict['bbox'] += [(float(xmin), float(ymin), float(xmax), float(ymax))]
                output_dict['label'] += [int(LABELS[labels[i]])]
                output_dict['score'] += [float(scores[i])]
        output += [output_dict]

    with open('submitRandRestrain152.json', 'w') as outfile:
        json.dump(output, outfile)


