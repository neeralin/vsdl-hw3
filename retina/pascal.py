from keras_retinanet.preprocessing.generator import Generator
import os
import numpy as np
from six import raise_from
from PIL import Image
import numpy as np
import pandas as pd
import h5py
"""
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
"""

svhn_classes = {
    '10'    : 0,
    '1'     : 1,
    '2'     : 2,
    '3'     : 3,
    '4'     : 4,
    '5'     : 5,
    '6'     : 6,
    '7'     : 7,
    '8'     : 8,
    '9'     : 9,
}


class PascalVocGenerator(Generator):
    """ Generate data for a Pascal VOC dataset.

    See http://host.robots.ox.ac.uk/pascal/VOC/ for more information.
    """

    def __init__(
        self,
        # data_dir,
        imgs_dir,
        anns_dir,
        imgs_names=None,
        classes=svhn_classes,
        image_extension='.png',
        skip_truncated=False,
        skip_difficult=False,
        **kwargs
    ):
        """ Initialize a Pascal VOC data generator.

        Args
            base_dir: Directory w.r.t. where the files are to be searched (defaults to the directory containing the csv_data_file).
            csv_class_file: Path to the CSV classes file.
        """
        # self.data_dir = data_dir
        self.classes = classes
        # self.image_names = [l.strip().split(None, 1)[0] for l in open(os.path.join(data_dir, 'ImageSets', 'Main', set_name + '.txt')).readlines()]

        self.imgs_dir = imgs_dir
        self.anns_dir = anns_dir
        
        if imgs_names is None:
            import glob
            imgs_names = glob.glob(imgs_dir + "/*.png")
            
        self.image_names = [os.path.basename(img)[:-4] for img in imgs_names]

        
        self.image_extension = image_extension
        self.skip_truncated = skip_truncated
        self.skip_difficult = skip_difficult

        self.labels = {}
        for key, value in self.classes.items():
            self.labels[value] = key

        super(PascalVocGenerator, self).__init__(**kwargs)

    def size(self):
        """ Size of the dataset.
        """
        return len(self.image_names)

    def num_classes(self):
        """ Number of classes in the dataset.
        """
        return len(self.classes)

    def has_label(self, label):
        """ Return True if label is a known label.
        """
        return label in self.labels

    def has_name(self, name):
        """ Returns True if name is a known class.
        """
        return name in self.classes

    def name_to_label(self, name):
        """ Map name to label.
        """
        return self.classes[name]

    def label_to_name(self, label):
        """ Map label to name.
        """
        return self.labels[label]

    def image_aspect_ratio(self, image_index):
        """ Compute the aspect ratio for an image with image_index.
        """
        path  = os.path.join(self.imgs_dir, self.image_names[image_index] + self.image_extension)
        image = Image.open(path)
        return float(image.width) / float(image.height)

    def load_image(self, image_index):
        """ Load an image at the image_index.
        """
        path = os.path.join(self.imgs_dir, self.image_names[image_index] + self.image_extension)
        return read_image_bgr(path)
    '''
    def __parse_annotation(self, element):
        """ Parse an annotation given an XML element.
        """
#         truncated = _findNode(element, 'truncated', parse=int)
#         difficult = _findNode(element, 'difficult', parse=int)

        class_name = _findNode(element, 'name').text
        if class_name not in self.classes:
            raise ValueError('class name \'{}\' not found in classes: {}'.format(class_name, list(self.classes.keys())))

        box = np.zeros((4,))
        label = self.name_to_label(class_name)

        bndbox    = _findNode(element, 'bndbox')
        box[0] = _findNode(bndbox, 'xmin', 'bndbox.xmin', parse=float) - 1
        box[1] = _findNode(bndbox, 'ymin', 'bndbox.ymin', parse=float) - 1
        box[2] = _findNode(bndbox, 'xmax', 'bndbox.xmax', parse=float) - 1
        box[3] = _findNode(bndbox, 'ymax', 'bndbox.ymax', parse=float) - 1

        return box, label

    def __parse_annotations(self, xml_root):
        """ Parse all annotations under the xml_root.
        """
        annotations = {'labels': np.empty((len(xml_root.findall('object')),)), 'bboxes': np.empty((len(xml_root.findall('object')), 4))}
        for i, element in enumerate(xml_root.iter('object')):
            try:
                box, label = self.__parse_annotation(element)
            except ValueError as e:
                raise_from(ValueError('could not parse object #{}: {}'.format(i, e)), None)

            annotations['bboxes'][i, :] = box
            annotations['labels'][i] = label

        return annotations
    '''

    def load_annotations(self, image_index):
        """ Load annotations for an image_index.
        """
        mat_file = os.path.join(self.anns_dir, "digitStruct.mat")
        f = h5py.File(mat_file, 'r')
        name = self.image_names[image_index]
        image = self.load_image(image_index)
        
        j = int(name)-1 
        img_name = get_name(j, f)
        row_dict = get_bbox(j, f)
        
        bbox_df = pd.DataFrame([], columns=['height', 'img_name', 'label', 'left', 'top', 'width'])
        if img_name.split('.')[0] == name:
            row_dict['img_name'] = img_name
            bbox_df = pd.concat([bbox_df, pd.DataFrame.from_dict(row_dict, orient='columns')])
            bbox_df['bottom'] = bbox_df['top'] + bbox_df['height']
            bbox_df['right'] = bbox_df['left'] + bbox_df['width']
        else:
            print("ERRORRRRRRR")

        img = {'labels':[], 'bboxes':[]}

        labels = []
        bboxes = []
        for index, b in bbox_df.iterrows():
            labels += [self.name_to_label(str(round(b['label'])))]
            
            if b['left'] < 0:                    ## invalid bbox problem
                b['left'] = 0
            if b['bottom'] > image.shape[0]:
                b['bottom'] = image.shape[0]
            if b['right'] > image.shape[1]:
                b['right'] = image.shape[1]
        
            bboxes += [[b['left'], b['top'], b['right'], b['bottom']]]

        label_np = np.array(labels)
        bboxes_np = np.array(bboxes)
        img['labels'] = label_np
        img['bboxes'] = bboxes_np
        
        return img
        '''
        filename = self.image_names[image_index] + '.xml'
        try:
            tree = ET.parse(os.path.join(self.anns_dir, filename))
            return self.__parse_annotations(tree.getroot())
        except ET.ParseError as e:
            raise_from(ValueError('invalid annotations file: {}: {}'.format(filename, e)), None)
        except ValueError as e:
            raise_from(ValueError('invalid annotations file: {}: {}'.format(filename, e)), None)
        '''

def _findNode(parent, name, debug_name=None, parse=None):
    if debug_name is None:
        debug_name = name

    result = parent.find(name)
    if result is None:
        raise ValueError('missing element \'{}\''.format(debug_name))
    if parse is not None:
        try:
            return parse(result.text)
        except ValueError as e:
            raise_from(ValueError('illegal value for \'{}\': {}'.format(debug_name, e)), None)
    return result


def read_image_bgr(path):
    """ Read an image in BGR format.

    Args
        path: Path to the image.
    """
    image = np.asarray(Image.open(path).convert('RGB'))
    return image[:, :, ::-1].copy()

def get_name(index, hdf5_data):
    name = hdf5_data['digitStruct/name']
    return ''.join([chr(v[0]) for v in hdf5_data[name[index][0]].value])

def get_bbox(index, hdf5_data):
    attrs = {}
    item = hdf5_data['digitStruct']['bbox'][index].item()
    for key in ['label', 'left', 'top', 'width', 'height']:
        attr = hdf5_data[item][key]
        values = [hdf5_data[attr.value[i].item()].value[0][0]
                  for i in range(len(attr))] if len(attr) > 1 else [attr.value[0][0]]
        attrs[key] = values
    return attrs

if __name__ == '__main__':
    
    dataset_path = "../samples"
    
    imgs_dir = "../../train"
    anns_dir = "../../train"
    
    import glob
    filenames = glob.glob(imgs_dir + "/*.png")
    
    import random
    random.seed(230)
    random.shuffle(filenames) # shuffles the ordering of filenames (deterministic given the chosen seed)
    split = int(0.8 * len(filenames))
    train_list = filenames[:split]
    valid_list = filenames[split:]
    generator = PascalVocGenerator(imgs_dir, anns_dir, valid_list)
    print(len(generator.image_names)) #['2', '1']
    print(generator.image_names[0:3])
    '''
    print(generator.classes)  # 'label name': index
    print(generator.imgs_dir) #../samples/JPEGImages
    print(generator.anns_dir) #../samples/Annotations
    
    print(generator.image_extension) #.png
    print(generator.skip_truncated) #false
    print(generator.skip_difficult) #false
    print(generator.labels) # index: 'label name'
    print(PascalVocGenerator.load_annotations(generator, 101))
    '''
